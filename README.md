There is JSON service from flicks:

http://www.flickr.com/services/feeds/photos_public.gne?tags=soccer&format=json&jsoncallback=?

The task is pretty common:

* Download JSON from the specified URL

* Present received date in tableView

* present small data in cells (thumb, title)

* present big data in detail view (description, html, web page)

* Save data on device with core data usage.

* (Do not use external libs. everything should be native)

* (objective с is preferable)

* (storyboard is ok, but views generated from code preferable)